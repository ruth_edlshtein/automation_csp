﻿using AutomationUtilsRepo.Pages;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.BusinessProcesses
{
    class CreateTicketBP
    {
        NewTicketPage newTicketPage = new NewTicketPage();
        public bool IsAttachmentValidationErrorAppears()
        {
            try
            {
                newTicketPage.GetFailedUploadError();
                return true;
            }
            catch( NoSuchElementException ex)
            {
                return false;
            }
        }
    }
}
