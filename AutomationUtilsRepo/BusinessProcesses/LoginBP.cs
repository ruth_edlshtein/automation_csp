﻿using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Infra.Factories;
using AutomationUtilsRepo.Infra.Objects;
using AutomationUtilsRepo.Pages;
using Seldat.AutomationTools.Infra.Helper;

namespace AutomationUtilsRepo.BusinessProcesses
{
    public class LoginBP
    {
        Login login = new Login();
        MainPage main = new MainPage();
        LoginPage loginPage = new LoginPage();
        public void Login(string userName, string password)
        {
            login.InsertEmail(userName);
            login.InsertPassword(password);
            login.ClickLogin();
        }

        public void Login(UserType userType)
        {
            User user = UsersFactory.Get(userType);
            Login(user.email, user.password);
        }

        public bool IsUserLoggedIn(string userName)//Need to be in the main screen before calling this function
        {
            if (FindElementHelper.GetText(main.GetLoggedUserName()).Contains(userName))
                return true;
            return false;
        }

        public bool IsErrorDisplay()
        {
            try
            {
                loginPage.GetErrorMessage();
                return true;
            }
            catch (OpenQA.Selenium.WebDriverTimeoutException)
            {
                return false;
            }
        }



    }
}
