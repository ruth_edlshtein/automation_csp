﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using AutomationUtilsRepo.Infra.Const;
using Seldat.AutomationTools.Infra.Const;

namespace AutomationUtilsRepo.DataBase
{
    public class ConnectionDB
    {

        private DataTable dataTable = new DataTable();

        public ConnectionDB()
        {
        }

        public DataTable QueryResult
        {
            get
            {
                return dataTable;
            }
        }

        //return the result of the query 
        public DataTable GetQueryResult(string queryName)
        {
            PullData(queryName);
            return QueryResult;

        }


        //pull data from DataBase\Queries.xml and running the query   
        public void PullData(string queryName)
        {
           // string queryName = "Q1";

            //Load the XML document into memory
            XmlDocument doc = new XmlDocument();
            doc.Load(GenericConstPath.QueriesPath);

            //get a Definition nodes in the document
            XmlNode node = doc.SelectSingleNode("/ConnectionToDataBase/Queries/Query[@name='" + queryName + "']");
            string query = node.InnerText;

            string connString = doc.SelectSingleNode("/ConnectionToDataBase/ConnectionString").InnerText;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dataTable);
            conn.Close();
            da.Dispose();
        }
    }
}
