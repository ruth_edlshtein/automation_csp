﻿using AutomationUtilsRepo.Pages;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    class Tickets
    {
        MainPage mainPage = new MainPage();
        NewTicketPage newTicketPage = new NewTicketPage();

        public void ClickOnCreateTicket()
        {
            FindElementHelper.ClickOnElement(mainPage.GetPlusButton());
        }

        public void ClickOnAttachFile()
        {
            FindElementHelper.ClickOnElement(newTicketPage.GetAttachButton());
        }
    }
}
