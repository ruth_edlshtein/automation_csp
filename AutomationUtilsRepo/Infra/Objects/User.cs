﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Objects
{
    public class User
    {
        public string email;
        public string password;
        public string name;

        public User(string email, string password, string name)
        {
            this.email = email;
            this.password = password;
            this.name = name;
        }
    }
}
