﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    public class MainPage
    {
        public IWebElement GetProfileArea()
        {
            return FindElementHelper.GetElement(By.XPath("//ul[@class='navbar-nav ml-auto profile']"));
        }

        public IWebElement GetProfileDropDown()
        {
            return FindElementHelper.GetElement(By.XPath("//*[@class='profile-avatar']/parent::a"));
        }

        public IWebElement GetLoggedUserName()
        {
            return FindElementHelper.GetElement(By.XPath("//span[contains(text(),'')]"));
        }

        public IWebElement GetPlusButton()
        {
            return FindElementHelper.GetElement(By.XPath("//button[@title='Create new ticket']"));
        }
    }
}
