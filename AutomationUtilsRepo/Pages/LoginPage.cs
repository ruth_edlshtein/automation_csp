﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    public class LoginPage
    {
        public IWebElement GetEmailInput()
        {
            IWebElement element =  FindElementHelper.GetElement(By.Id("inp1"));
            return element;
        }

        public IWebElement GetPasswordInput()
        {
            return FindElementHelper.GetElement(By.Id("inp"));
        }

        public IWebElement GetContinueButton()
        {
            return FindElementHelper.GetElement(By.Id("login"));
        }

        public IWebElement GetErrorMessage()
        {
            return FindElementHelper.GetElement(By.ClassName("p-error"));
        }
    }
}
