﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    class NewTicketPage
    {
        public IWebElement GetTitle()
        {
            return FindElementHelper.GetElement(By.XPath("//b[text()='Create new ticket']"));
        }

        public IWebElement GetAttachButton()
        {
            return FindElementHelper.GetElement(By.XPath("//button[@title='Attach a file']"));
        }

        public IWebElement GetFailedUploadError()
        {
            return FindElementHelper.GetElementById("cdk-overlay-0");
        }
    }
}
