﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Infra.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    [Category("LoginTests")]
    [Category("FinalRegression")]
    class LoginTests : SeleniumTestBase
    {
        LoginBP loginBP = new LoginBP();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        bool isTrue;


        //Test No. 4
        [Test]
        public void LoginAsAdminTest()
        {
            try
            {
                loginBP.Login(UserType.User);
                isTrue = loginBP.IsUserLoggedIn(UsersFactory.Get(UserType.User).name);
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 5
        [Test]
        public void LoginOnlyValidUserNameTest()
        {
            try
            {
                loginBP.Login("ruth.edlshtein@seldatinc.com", GeneralHelper.RandomStringOfLetters(5));
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 7
        [Test]
        public void LoginOnlyValidPasswordTest()
        {
            try
            {
                loginBP.Login("nonExistUser@gmail.com", "Aa123!");
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 8
        [Test]
        public void LoginIncompatibleUserNameAndPasswordTest()
        {
            try
            {
                loginBP.Login("nonExistUser@gmail.com", GeneralHelper.RandomStringOfLetters(5));
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 9
        [Test]
        public void LoginValidDatailsAsButNotExistUserTest()
        {
            try
            {
                loginBP.Login("nonExistUser@gmail.com", GeneralHelper.RandomStringOfLetters(5));
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 10
        [Test]
        public void LoginValidDatailsAsButNotExistUserNameTest()
        {
            try
            {
                loginBP.Login("nonExistUser@gmail.com", "Aa123!");
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 11
        [Test]
        public void LoginValidUserNameWithoutPasswordTest()
        {
            try
            {
                loginBP.Login("nonExistUser@gmail.com", "");
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 12
        [Test]
        public void LoginValidPasswordWithoutUserNameTest()
        {
            try
            {
                loginBP.Login("", "Aa123!");
                isTrue = loginBP.IsErrorDisplay();
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }


        ////Test No. 13
        //[Test]
        //public void LoginValidUserNameWithoutPasswordTest()
        //{
        //    try
        //    {
        //        loginBP.Login("nonExistUser@gmail.com", GeneralHelper.RandomStringOfLetters(5));
        //        isTrue = loginBP.IsErrorDisplay();
        //        stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
        //    }
        //    catch (Exception ex)
        //    {
        //        stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
        //    }
        //}


    }
}
