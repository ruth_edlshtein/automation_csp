﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    class MainScreenTest : SeleniumTestBase
    {
        LoginBP loginBP = new LoginBP();
        MainScreenBP mainScreenBP = new MainScreenBP();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        bool isTrue;        

        //Test No. 1
        [Test]
        public void EnterToMainPage()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='col s6 button-selected' and contains(text(),'Home')]"));
                if (element.Text == "Home")
                {
                    isTrue = true;
                }               
                 
                stepReport.Report("Enter to main page", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 2
        [Test]
        public void TopBarTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Check if Top Navigation Bar exist", "Top Navigation Bar verified", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 3 a
        [Test]
        public void ButtonNewTicketTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 3 b
        [Test]
        public void ButtonNewTicketToolTipTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 4
        [Test]
        public void ButtonNewTicketClickTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 5 a
        [Test]
        public void CaptionAtHeaderTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 5 b
        [Test]
        public void CaptionAtSubHeaderTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 6
        [Test]
        public void AppearsCardsTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 7
        [Test]
        public void OrderOfCardsTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 8
        [Test]
        public void SupportStandardTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
        //Test No. 9
        //Test No. 10
        [Test]
        public void NumberOfOpenTicketsTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 11
        [Test]
        public void ClickOnTicketNumberTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 11
        [Test]
        public void LoginValidUserNameWithoutPasswordTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        //Test No. 12
        [Test]
        public void EveryCustomerTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }


        //Test No. 13 a
        [Test]
        public void TheURLWasMovedDependingTheImageClickTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }


        //Test No. 13 b 
        [Test]
        public void TheURLWasMovedDependingTheProductClickTest()
        {
            try
            {
                mainScreenBP.Login("ruth.edlshtein@seldatinc.com", "Aa123!");
                IWebElement element = FindElementHelper.GetElement(By.XPath("//button[@class='fas fa-comment-alt-plus']"));
                if (element.Text == "Home" && element.Selected)
                {
                    isTrue = true;
                }
                stepReport.Report("Step1- Login as non exist user", "error message is displayed to user.", reportStatus.Status(isTrue));
            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }
    }



}
