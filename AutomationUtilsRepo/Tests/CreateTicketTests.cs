﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Const;
using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Infra.Factories;
using AutomationUtilsRepo.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    class CreateTicketTests:SeleniumTestBase
    {
        LoginBP loginBP = new LoginBP();
        Tickets tickets = new Tickets();
        bool isTrue;
        ReportStatus reportStatus = new ReportStatus();
        NewTicketPage newTicketPage = new NewTicketPage();
        CreateTicketBP createTicketBP = new CreateTicketBP();

        [Test]
        public void NewTicketPageTest()
        {
            try
            {
                //login
                loginBP.Login(UserType.User);
                isTrue= loginBP.IsUserLoggedIn(UsersFactory.Get(UserType.User).name);
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));

                //click on create new ticket and verify you are in create ticket page.
                tickets.ClickOnCreateTicket();
                isTrue = DriverHelper.GetUrl().Contains("https://mail.google.com");
                stepReport.Report("Step2- Click on create  ticket button", "Page of create ticket is opened.", reportStatus.Status(isTrue));

                //Verify Header
                try
                {
                    newTicketPage.GetTitle();
                }
                catch (NoSuchElementException ex)
                {
                    isTrue = false;
                }
                stepReport.Report("Step3- Verify header exists.", "There is title of create new ticket.", reportStatus.Status(isTrue));

                //Verify phone header 
                //TODO

            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }
        }

        [Test]
        public void AttachmentValidationTest()
        {
            try {
                //login
                loginBP.Login(UserType.User);
                isTrue = loginBP.IsUserLoggedIn(UsersFactory.Get(UserType.User).name);
                stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));

                //Navigate to new ticket page
                tickets.ClickOnCreateTicket();
                Thread.Sleep(5000);
                isTrue = DriverHelper.GetUrl().Contains("/ticket/ticket-list/true");
                stepReport.Report("Step2- Click on create  ticket button", "Page of create ticket is opened.", reportStatus.Status(isTrue));

                //Attach file more than 5MB
                tickets.ClickOnAttachFile();
                Thread.Sleep(3000);
                GeneralHelper.BrowseFile(ConstPath.HeavyFilePath);
                Thread.Sleep(3000);
                stepReport.Report("Step3- Upload File more than 5 mb", "Validation error appears.", reportStatus.Status(createTicketBP.IsAttachmentValidationErrorAppears()));

            }
            catch (Exception ex)
            {
                stepReport.Report("Test was failed because of an exception.", ex.StackTrace, LogStatus.Fail);
            }

        }
    }
}
