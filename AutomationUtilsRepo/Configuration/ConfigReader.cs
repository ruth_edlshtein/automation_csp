﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AutomationUtilsRepo.Configuration
{
   public class ConfigReader
    {

        //return the enviroment link from app.config
        public static string GetEnviroment()
        {
            return ConfigurationManager.AppSettings["EnviromentURL"].ToString();
        }

        public static int GetRetryMaxTimes()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["RetryMaxTimes"].ToString());
        }
    }
}
